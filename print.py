def fact(i):
    r = 1
    for n in range(2, i+1):
        r *= n
    return r

print(fact(100))
